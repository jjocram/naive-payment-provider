# Naive Payment Provider
This java application is a web-service for a naive payment provider. 

## How to run
### Jar file
You need Java 1.8 (or higher) to execute it.
From terminal run the following command:

```java -jar NaivePaymentProvider-1.1.0.jar```

It has been tested on: MacOS, Windows, Ubuntu

### Maven
You can run it from sources with Maven.
From terminal run the following command inside the directory

```mvn spring-boot:run```

### IntelliJ
You can import this project in IntelliJ. It should be recognised as a Spring-Boot project, and you should be able to 
just press "Play".

### Other IDEs
Other IDEs should be capable of understanding the project structure and propose a working "Play" button.

## How to use it
This is a web-service. It runs locally on your computer, and it is reachable with an HTTP request. 
The only HTTP request you can make is a POST to the following url:
```
http://0.0.0.0:6789/pay
```
To interact with it in your project, you can start with searching "How to make a POST in `XYZ`" where `XYZ` is the language/framework you are using.

### Request
The request body is a JSON with 4 keys:
- `amount: Double` = the amount of money required
- `card_holder: String` = name and surname of the credit card holder
- `card_number: String` = the credit card number
- `expire_date: String` = the expire date of the credit card
- `cvv: String` = the three numbers on the back of the credit card

An example is the following:
```json
{
  "amount": 12.3,
  "card_holder": "Marco Ferrati",
  "card_number": "4263982640269299",
  "expire_date": "02/26",
  "cvv": "837"
}
```

You can try it with [cURL](https://curl.se) 
```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"amount": 12.3, "card_holder": "Marco Ferrati", "card_number": "4263982640269299", "expire_date": "02/26", "cvv": "837"}' \
  http://localhost:6789/pay
```

Or any other HTTP requests tester ([Postman](https://www.postman.com), [Hoppscotch](https://hoppscotch.io), ...).

### Response
The response is a JSON.

#### Happy path
If the request was well formatted the response will have 4 keys:
- `status: String` = `Autorizzato` or `Fallito`
- `total: Double` = the total amount of money taken
- `fee: Double` = the fee applied by the payment provider
- `message: String` = a message related to the status

An example is the following:
```json
{
  "status": "Autorizzato",
  "total": 10.81,
  "fee": 0.81,
  "message": "Abbiamo ricevuto il tuo pagamento con successo."
}
```
```json
{
  "status": "Fallito",
  "total": 10.86,
  "fee": 0.86,
  "message": "Purtroppo, il sistema non è riuscito a elaborare il tuo pagamento."
}
```

#### Something went wrong
If the request was not in the right format or any other error occurs, the response will be a JSON with 4 keys:
- `timestamp: String`
- `status: Int` = the HTTP Status. A help to understand what happened can be found at the following [link](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
- `error: String` = the description of the HTTP Status error code. A help to understand what happened can be found at the following [link](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
- `message: String` = the description of the error
- `errors: Optional<[Object]>` = the list with all the errors occurred. Each entry gives more information
- `path: String` = the path at which the request was made 

An example is the following:

If you send an incomplete JSON like
```json
{
  "card_holder": "Marco",
  "card_number": "123456778"
}
```

The response will be
```json
{
  "timestamp": "2023-11-27T14:05:44.738+00:00",
  "status": 400,
  "error": "Bad Request",
  "message": "Validation failed for object='paymentRequest'. Error count: 1",
  "errors": [
    {
      "codes": [
        "NotNull.paymentRequest.amount",
        "NotNull.amount",
        "NotNull.java.lang.Double",
        "NotNull"
      ],
      "arguments": [
        {
          "codes": [
            "paymentRequest.amount",
            "amount"
          ],
          "arguments": null,
          "defaultMessage": "amount",
          "code": "amount"
        }
      ],
      "defaultMessage": "amount has to be present",
      "objectName": "paymentRequest",
      "field": "amount",
      "rejectedValue": null,
      "bindingFailure": false,
      "code": "NotNull"
    }
  ],
  "path": "/pay"
}
```

If you send a malformed json like (missing comma after `10`):
```json
{
  "amount": 10
  "card_holder": "Marco",
  "card_number": "123456778",
  "cvv": "123"
}
```

The response will be
```json
{
  "timestamp": "2023-11-27T14:06:50.583+00:00",
  "status": 400,
  "error": "Bad Request",
  "message": "JSON parse error: Cannot construct instance of `com.marcoferrati.NaivePaymentProvider.PaymentRequest` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value ('{\n  \"amount\": 10\n  \"card_holder\": \"Marco\",\n  \"card_number\": \"123456778\",\n  \"cvv\": \"123\"\n}'); nested exception is com.fasterxml.jackson.databind.exc.MismatchedInputException: Cannot construct instance of `com.marcoferrati.NaivePaymentProvider.PaymentRequest` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value ('{\n  \"amount\": 10\n  \"card_holder\": \"Marco\",\n  \"card_number\": \"123456778\",\n  \"cvv\": \"123\"\n}')\n at [Source: (org.springframework.util.StreamUtils$NonClosingInputStream); line: 1, column: 1]",
  "path": "/pay"
}
```

If you send a request to the wrong endpoint (`/paga`), the response will be:
```json
{
  "timestamp": "2023-11-27T14:10:21.300+00:00",
  "status": 404,
  "error": "Not Found",
  "message": "No message available",
  "path": "/paga"
}
```