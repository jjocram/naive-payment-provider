package com.marcoferrati.NaivePaymentProvider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PaymentResponse {
    String status;
    double total;
    double fee;
    String message;
}
