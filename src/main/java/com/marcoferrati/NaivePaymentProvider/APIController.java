package com.marcoferrati.NaivePaymentProvider;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Random;

@RestController
public class APIController {
    public static final String[] messages_ok = {
            "Il tuo pagamento è stato elaborato con successo.",
            "La transazione è andata a buon fine, il tuo pagamento è stato accettato.",
            "Confermiamo che il tuo pagamento è stato riuscito con successo.",
            "Pagamento completato con successo, grazie per la tua transazione.",
            "Il tuo pagamento è stato autorizzato con successo e la transazione è stata completata.",
            "Abbiamo ricevuto il tuo pagamento con successo.",
            "La tua transazione è stata confermata, il pagamento è stato effettuato con successo.",
            "Congratulazioni! Il tuo pagamento è stato registrato con successo nel nostro sistema.",
            "La tua transazione è andata a buon fine, il pagamento è stato completato con successo.",
            "tuo pagamento è stato elaborato con successo, grazie per la tua transazione."};

    public static final String[] messages_fail = {
            "Ci dispiace informarti che il pagamento non è andato a buon fine.",
            "Siamo spiacenti, ma la tua transazione non è stata completata con successo.",
            "Il pagamento che hai tentato di effettuare non è stato autorizzato.",
            "Ci scusiamo, ma sembra che ci siano stati problemi con il tuo pagamento.",
            "Purtroppo, il sistema non è riuscito a elaborare il tuo pagamento.",
            "Il pagamento è stato respinto, ti preghiamo di verificare le informazioni inserite.",
            "Siamo spiacenti di comunicare che il tuo tentativo di pagamento non ha avuto successo.",
            "La tua transazione è stata annullata in quanto il pagamento non è andato a buon fine.",
            "Il sistema ha riscontrato un errore durante il processo di pagamento, per favore riprova.",
            "Spiacenti, ma la tua carta di pagamento è stata rifiutata per questa transazione."
    };

    @GetMapping("/heartbeat")
    public ResponseEntity<String> live() {
        return ResponseEntity.ok("Live");
    }

    @PostMapping(value = "/pay", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentResponse pay(@Valid @RequestBody PaymentRequest paymentRequest) {
        double fee = (double) Math.round(Math.random() * 100) / 100;
        double total = paymentRequest.getAmount() + fee;

        boolean status = (new Random()).nextBoolean();
        String statusStr = status ? "Autorizzato" : "Fallito";
        int randomIndex = (new Random()).nextInt(9);
        String randomMessage = status ? messages_ok[randomIndex] : messages_fail[randomIndex];

        return new PaymentResponse(statusStr, total, fee, randomMessage);
    }
}
