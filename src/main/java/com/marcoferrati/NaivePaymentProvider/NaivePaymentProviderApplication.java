package com.marcoferrati.NaivePaymentProvider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NaivePaymentProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(NaivePaymentProviderApplication.class, args);
	}
}
