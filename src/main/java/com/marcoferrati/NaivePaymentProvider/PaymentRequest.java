package com.marcoferrati.NaivePaymentProvider;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class PaymentRequest {

    @NotNull(message = "amount has to be present")
    @Min(value = 0, message = "amount has to be greater than 0")
    @JsonProperty(value = "amount", required = true)
    Double amount;

    @NotEmpty(message = "card_holder has to be present")
    @JsonProperty(value = "card_holder", required = true)
    String cardHolder;

    @NotEmpty(message = "card_number has to be present")
    @JsonProperty(value = "card_number", required = true)
    String cardNumber;

    @NotEmpty(message = "card_cvv has to be present")
    @JsonProperty(value = "cvv", required = true)
    String cvv;
}
